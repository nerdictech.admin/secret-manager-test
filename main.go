package main

import (
	"context"
	"log"

	vault "github.com/hashicorp/vault/api"
)

func main() {
	// Initialize the Vault configuration
	config := vault.DefaultConfig()

	// Set the address of the Vault server
	config.Address = "http://127.0.0.1:8200"

	// Set the address of the Vault server
	client, err := vault.NewClient(config)
	if err != nil {
		log.Fatalf("unable to initialize Vault client: %v", err)
	}

	// Set the token to authenticate with the Vault server
	client.SetToken("dev-only-token")

	// Define the secret data to be stored
	secretData := map[string]interface{}{
		"password": "Hashi123",
	}

	ctx := context.Background()

	// Write the secret to the Vault server
	_, err = client.KVv2("secret").Put(ctx, "my-secret-password", secretData)
	if err != nil {
		log.Fatalf("unable to write secret: %v", err)
	}

	log.Println("Secret written successfully.")

	// Read the secret from the Vault server
	secret, err := client.KVv2("secret").Get(ctx, "my-secret-password")
	if err != nil {
		log.Fatalf("unable to read secret: %v", err)
	}

	// Extract the password value from the retrieved secret and validate
	value, ok := secret.Data["password"].(string)
	if !ok {
		log.Fatalf("value type assertion failed: %T %#v", secret.Data["password"], secret.Data["password"])
	}

	if value != "Hashi123" {
		log.Fatalf("unexpected password value %q retrieved from vault", value)
	}
	log.Println("Access granted!")
}
