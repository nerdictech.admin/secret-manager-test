# secret-manager-test
The SECRET_MANAGER_TEST repository is focused on exploring and demonstrating the capabilities of Vault, a tool specialized in encryption and secure password management. Through this space, practical examples will be provided that will show how to configure, encrypt, and manage passwords with Vault.

This repository is part of a series of internal initiatives by Nerdictech to ensure maximum security in the management of sensitive data.

## Requirement
1.- Have the Vault package previously installed and configured.
2.- Golang version 1.18 installed.
3.- Have Go modules set up.
4.- Docker installed and configured (Recommended to start it before running the program).

## Execution

1.-Start the Vault server in development mode from the PowerShell terminal by executing the following command:

    $container_id = docker run --rm --detach -p 8200:8200 -e 'VAULT_DEV_ROOT_TOKEN_ID=dev-only-token' vault:1.11.0
    
    Explanation:
    docker run: Starts a new container.
    --rm: Ensures the container is automatically removed once it's stopped.
    --detach: Allows the container to run in the background.
    -p 8200:8200: Maps the container's port 8200 to port 8200 on your machine.
    -e 'VAULT_DEV_ROOT_TOKEN_ID=dev-only-token': Sets a root token for the Vault server in development mode.
    vault:1.11.0: Specifies the version of the Vault image you want to run.

2.-Execute the example code from our terminal:

    go run main.go

3.-Stop the Vault server from the PowerShell terminal by executing the following command:
    
    docker stop "${container_id}" > $null

    Explanation:
    docker stop: Used to stop a running container.
    "${container_id}": Refers to the container ID that we stored in the first step. This ensures we stop the correct container.

## Reference
1.-Configurate Go modules: https://nerdictech.atlassian.net/wiki/spaces/IDH/pages/7012353/Go+modules
